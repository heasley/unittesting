package com.interapt.testingexample;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

  @BindView(R.id.first_name_text_input_layout) protected TextInputLayout firstNameTIL;
  @BindView(R.id.last_name_text_input_layout) protected TextInputLayout lastNameTIL;
  @BindView(R.id.email_name_text_input_layout) protected TextInputLayout emailTIL;

  @BindView(R.id.first_name_edit_text) protected EditText firstNameEditText;
  @BindView(R.id.last_name_edit_text) protected EditText lastNameEditText;
  @BindView(R.id.email_edit_text) protected EditText emailEditText;

  @BindView(R.id.submit_button) protected Button submitButton;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);
  }
}
